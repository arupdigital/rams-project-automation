<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RAMs Project Automation</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo SITEPATH ?>/assets/css/common/reset.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?php echo SITEPATH ?>/assets/css/lib/material.css">
    <link rel="stylesheet" href="<?php echo SITEPATH ?>/assets/css/common/datepicker.scss">
    <link rel="stylesheet" href="<?php echo SITEPATH ?>/assets/css/lib/material-datetime-picker.css">
    <link rel="stylesheet" href="<?php echo SITEPATH ?>/assets/css/lib/swiper.min.css">
    <link rel="stylesheet" href="<?php echo SITEPATH ?>/assets/css/common/main.css">
</head>
<body>
    <main class="mdl-layout__content">
