    </main>
    <script src="<?php echo SITEPATH ?>/assets/js/lib/jquery-2.2.4.min.js"></script>
    <script src="<?php echo SITEPATH ?>/assets/js/lib/swiper.min.js"></script>
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="https://unpkg.com/babel-polyfill@6.2.0/dist/polyfill.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rome/2.1.22/rome.standalone.js"></script>
    <script src="<?php echo SITEPATH ?>/assets/js/lib/datepicker.js"></script>
    <script src="<?php echo SITEPATH ?>/assets/js/common/main.js"></script>
</body>
</html>