
  <div class="mdl-layout__drawer">
    <a style='text-align: center;' href="<?php echo SITEPATH ?>/"><img src="<?php echo SITEPATH ?>/assets/img/logo.svg" alt="" id="navlogo"></a>
    <br/>
    <span id='subtitle'>RAMs Project Automation</span>
    <nav class="mdl-navigation">
      <?php
      foreach($GLOBALS['nav'] as $navItem) {
        if($GLOBALS['active']==$navItem['name']){
          echo '<a class="active mdl-navigation__link" href="'. SITEPATH .'/'. $navItem['url'].'/">'.$navItem['name'].'</a>';
        }else{
          echo '<a class="mdl-navigation__link" href="'. SITEPATH .'/'. $navItem['url'].'/">'.$navItem['name'].'</a>';
        }
      }
      ?>
    </nav>
  </div>
