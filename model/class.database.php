<?php

class DatabaseConnection
{

    public $conn = null;

    public function connectDB(){
        try {
            $this->conn = new PDO("pgsql:host=rams.c15wexuav8kw.ca-central-1.rds.amazonaws.com;dbname=rams;port=5432;options='--client_encoding=UTF8'", "RAMs", "postgres");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            $this->conn = null;
            die ("Major Error" . $e->getMessage());
        }

       return $this->conn;
    }

    /**
		* Dynamic SQL handler
		* @param $args : array of values for col=val
		* @param $action : Select, Update, Delete, Insert
		* @param $target : specific for where -> col / val
		* @param $table : the table in question
		* @param $_FILES : if there are files attached
	*/
  	public function sqlHandler($arguments){
		$args = $arguments['args'];
		$action = $arguments['action'];
		$target = $arguments['target'];
		$table = $arguments['table'];
		$file = $arguments['file'];

  		if($file!=""){
			if($file['image']['tmp_name']!=""){
				$imgOBJ = new Imagery;
				$image = $imgOBJ->imageHandler($file);
				$args['image'] = $image;
			}
		}

  		//default values for dynamic query
  		$selection= '';
  		$setSelection= '';
  		$newSet= '';
  		$insertSelectionA= '(';
  		$insertSelectionB= ')VALUES(';
  		$whereSet= '';
  		$exeData=array();

  		//dynamic value injector
  		$i= 0;
      	if(is_array($args)){
			$containsAll = 0;
    		foreach($args as $key=>$arg){
				if($action=="insert"||$action=="update"){
					if(is_string($arg)){
						$arg = str_replace("'", '\'', $arg);
					}
				}

    			//params in question : SELECT scenario
    			$selection .= "$key, ";

          		//execute array : INSERT scenario
    			$exeData[$key] = $arg;

    			//col to val : UPDATE scenario / DELETE scenario
    			(is_string($arg) ? $arg = "'$arg'" : $arg );
    			$setSelection .= "$key = $arg, ";

    			//listing cols : INSERT scenario
    			if($i==count($args)){
    				$insertSelectionA .= "$key";
    				$insertSelectionB .= ":$key";
    			}else{
    				$insertSelectionA .= "$key, ";
    				$insertSelectionB .= ":$key, ";
    			}

    			//Where target(s) : ALL scenarios
          		if($target!=""){
      				if(in_Array($key, $target) && strpos($arg, '*') === false){
      					(is_string($arg) ? $arg = "$arg" : $arg );
      					$whereSet .= "$key = $arg AND ";
      				}
				}
				if(strpos($arg, '*') !== false){
					$containsAll=1;
				}
    			$i++;
			}

			//Clean up of Strings
			if($containsAll==0){
				$selection = substr($selection ,0 ,-2);
			}else{
				$selection = '*';
			}
    		$setSelection = substr($setSelection ,0 ,-2);
    		$whereSet = substr($whereSet ,0 ,-5);

        	$insertSelectionA = substr($insertSelectionA ,0 ,-2);
        	$insertSelectionB = substr($insertSelectionB ,0 ,-2);
    		$newSet = $insertSelectionA . $insertSelectionB . ")";
      	}

  		//action for query to the DB
  		switch($action){
  			case 'select':
  				$dbAction = "SELECT";
  				$where = ($whereSet != "" ? "WHERE $whereSet" : "");
  				$q = "%s %s FROM %s %s ORDER BY id ASC";
          		$exeData = '';
          		if($selection==""){$selection="*";$where="WHERE active = 1";}
  				$queryInfo = sprintf($q, ($dbAction), ($selection), ($table), ($where));
			break;

			case 'select distinct':
  				$dbAction = "SELECT DISTINCT";
  				$where = ($whereSet != "" ? "WHERE $whereSet" : "");
  				$q = "%s %s FROM %s %s";
          		$exeData = '';
          		if($selection==""){$selection="*";$where="WHERE active = 1";}
  				$queryInfo = sprintf($q, ($dbAction), ($selection), ($table), ($where));
  			break;

  			case 'update':
  				$dbAction = "UPDATE";
  				$where = ($whereSet != "" ? "WHERE $whereSet" : "");
  				$q = "%s %s SET %s %s";
  				$exeData = $exeData;
  				$queryInfo = sprintf($q, ($dbAction), ($table), ($setSelection), ($where));
  			break;

  			case 'insert':
  				$dbAction = "INSERT INTO";
  				$where = ($whereSet != "" ? "WHERE $whereSet" : "");
  				$q = "%s %s %s";
  				$exeData = $exeData;
  				$queryInfo = sprintf($q, ($dbAction), ($table), ($newSet));
  			break;

  			case 'delete':
  				$dbAction = "DELETE";
  				$q = "%s FROM %s WHERE %s";
          		$exeData = '';
  				$queryInfo = sprintf($q, ($dbAction), ($table), ($whereSet));
  			break;
  		}

		/*echo "<pre>";
		var_dump($queryInfo);
		//var_dump($exeData)."<br /><br />";
		echo "</pre>";
		echo "<br/><Br />";*/

  		//Run transaction and sql query
  		$this->conn->beginTransaction();
  		try{
  			$result = $this->conn->prepare($queryInfo);
  			$result->setFetchMode(PDO::FETCH_ASSOC);

  			if($exeData!=""&&$action=='insert'){
  				$result->execute($exeData);
  			}else{
  				$result->execute();
			}
			
        	$result = $result->fetchAll();
			$this->conn->commit();
			  
			if($action=='insert'){
				$result=true;
			}
  		}catch(PDOException $e){
  			$this->conn->rollBack();

			//unset image that was created if failed
			if($file!=""){
				unset($args['image']);
			}
			echo $e->getMessage();

			if($action=='insert'){
				$result=false;
			}
		}

		if($file!=""){
			return $args['image'];
		}else{
			return $result;
		}
		//exit;
	}
}
?>
