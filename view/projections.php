<?php
    include('./controller/controller.projections.php');
?>

<section id="projections">
  <h1 class="header"><?php echo $GLOBALS['active']; ?></h1>
  <div class="header-descript">
    <p>This is just a little explanation of what this section is about and what you are doing here.</p>

    <form action="" method="post">
      <div class="row" style="justify-content:start;">
        <select class="mdl-textfield__input" id="session_id" name="session_id">
          <?php
          if(isset($sessionId)){
            echo '<option value="'.$sessionId.'">'.$sessionId.'</option>';
          }
          foreach($sessions as $a=>$b){
            if(isset($sessionId) && $sessionId != $b['session_id']){
              echo '<option value="'.$b['session_id'].'">'.$b['session_id'].'</option>';
            }else if(!isset($sessionId)){
              echo '<option value="'.$b['session_id'].'">'.$b['session_id'].'</option>';
            }
          }
          ?>
        </select>
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
          Select Session
        </button>
        <?php
          if(isset($tables)){
        ?>
        <a href="<?php echo SITEPATH; ?>/export/<?php echo $sessionId; ?>" target="_blank" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="margin-left:20px;">
            Export Session for P6
        </a>
        <?php
          }
        ?>
      </div>
    </form>
  </div>

  <?php
  if(isset($tables)){
  ?>
  <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
      <!-- Tabs -->
      <div class="mdl-layout__tab-bar mdl-js-ripple-effect">
        <a href="#scroll-tab-1" class="mdl-layout__tab is-active">Task</a>
        <a href="#scroll-tab-2" class="mdl-layout__tab">Taskrsrc</a>
        <a href="#scroll-tab-3" class="mdl-layout__tab">Rsrc</a>
      </div>
    </header>
    <main class="mdl-layout__content" style="overflow:hidden;">
      <?php
      foreach($tables as $arrKey=>$d){
        $act='';
        if($arrKey==0)
          $act='is-active';
      ?>
      <div class="mdl-layout__tab-panel <?php echo $act; ?>" id="scroll-tab-<?php echo ((int)$arrKey+1); ?>">
        <div class="page-content" style="overflow: scroll;position: relative;display: block;height: 100%;max-height: calc(100vh - 420px);">
          <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
            <?php 
            // loop for data Values
              //unset($d[0]);
              foreach($d as $i=>$row){
                if($i==0){
                  echo "<tr style='height:49px;overflow:hidden;'>";
                  echo "<th>Edit</th>";
                }else{
                  echo "<tr style='height:0px;overflow:hidden;'>";
                  echo "<td><a href='".SITEPATH."/projection/".$row['id']."/".((int)$arrKey+1)."'><button class='mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent'>Edit</button></a></td>";
                }
                foreach($row as $h=>$val){
                  if(!in_array($h, $forDB)){
                    if($i==0){
                      echo "<th>".$val."</th>";
                    }else{
                      echo "<td>".$val."</td>";
                    }
                  }
                }
                echo "</tr>";
              }
            ?>
          </table>
        </div>
      </div>
      <?php
      }
      ?>

    </main>
  </div>
  <?php
  }
  ?>
</section>


