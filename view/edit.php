<?php
    include('./controller/controller.edit.php');
?>

<section id="projections">
    <h1 class="header">Edit</h1>
    <div class="header-descript">
        <p>This is just a little explanation of what this section is about and what you are doing here.</p>
    </div>

    <form id="editForm" action="" method="post">
        <input type="hidden" name="action" value="edit" />
        <?php
        $count=1;
        echo "<div class='row'>";
        foreach($result[0] as $head=>$value){
            if(!in_array($head, $forDB)){
                if(is_string($value)){
                    if(strpos($head,'date') !== false){ // Date
                        echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" data-type="date" type="text" id="'.$head.'" name="'.$head.'" value="'.$value.'">
                            <label class="mdl-textfield__label" for="'.$head.'">'.ucwords(str_replace('_', ' ', $head)).'</label>
                        </div>';
                    }else{ // String
                        echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="'.$head.'" name="'.$head.'" value="'.$value.'">
                            <label class="mdl-textfield__label" for="'.$head.'">'.ucwords(str_replace('_', ' ', $head)).'</label>
                        </div>';
                    }
                }else{ // Number
                    echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="number" id="'.$head.'" name="'.$head.'" pattern="-?[0-9]*(\.[0-9]+)?" value="'.(int)$value.'">
                            <label class="mdl-textfield__label" for="'.$head.'">'.ucwords(str_replace('_', ' ', $head)).'</label>
                            <span class="mdl-textfield__error">Input is not a number!</span>
                        </div>';
                }

                if($count % 3 === 0){
                    echo "</div><div class='row'>";
                }
                $count++;
            }
        }
        echo "</div>";
        ?>
        <br /><br />
        <div class='row'>
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
            Save Changes
            </button>
        </div>
    </form>
    <div id="material-header-holder" style="display:none">
        <div class="ui-datepicker-material-header">
            <div class="ui-datepicker-material-day">
            </div>
            <div class="ui-datepicker-material-date">
                <div class="ui-datepicker-material-month">
                </div>
                <div class="ui-datepicker-material-day-num">
                </div>
                <div class="ui-datepicker-material-year">
                </div>
            </div>
        </div>
    </div>
</section>