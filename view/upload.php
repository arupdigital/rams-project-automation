<?php
    include('./controller/controller.upload.php');
?>

<section>
  <h1 class="header"><?php echo $GLOBALS['active']; ?></h1>
  <div class="header-descript">
    <p>Please locate your exported .xls file from P6 and click the 'Choose File' button below. Your file will begin processing into the database.</p>
  </div>

  <br />
  <form method='POST' id='theForm' enctype='multipart/form-data'>
    <input type='hidden' name='action' value='bulk' />
    <div class='form-group'>
      <!--<label for='item' class='cols-sm-2 control-label'>* 2mb Limit</label>-->
      <input type='file' name='file' id='file' accept='.xls' />
    </div>
    <br/>
    <div class='form-group'>
      <button name='submit' type='submit' class='mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored'>Upload xls</button>
    </div>
    <div id='errorMessage'><?php echo $msg ?></div>
  </form>
</section>