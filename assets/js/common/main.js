$(document).ready(function(){
    $(document).on('change', '#switch-1', toggleProjType);

    var swiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });

        var headerHtml = $("#material-header-holder .ui-datepicker-material-header");

        var changeMaterialHeader = function(header, date) {
        var year   = date.format('Y');
        var month  = date.format('M');
        var dayNum = date.format('D');
        var isoDay = date.isoWeekday();
        
            var weekday = new Array(7);
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            weekday[7]=  "Sunday";

        $('.ui-datepicker-material-day', header).text(weekday[isoDay]);
        $('.ui-datepicker-material-year', header).text(year);
        $('.ui-datepicker-material-month', header).text(month);
        $('.ui-datepicker-material-day-num', header).text(dayNum);
        };

        $.datepicker._selectDateOverload = $.datepicker._selectDate;
        $.datepicker._selectDate = function(id, dateStr) {
        var target = $(id);
        var inst = this._getInst(target[0]);
        inst.inline = true;
        $.datepicker._selectDateOverload(id, dateStr);
        inst.inline = false;
        this._updateDatepicker(inst);
        
        headerHtml.remove();
        $(".ui-datepicker").prepend(headerHtml);
        };

        $("input[data-type='date']").on("focus", function() {
        //var date;
        //if (this.value == "") {
        //  date = moment();
        //} else {
        //  date = moment(this.value, 'MM/DD/YYYY');
        //}

            $(".ui-datepicker").prepend(headerHtml);
        //$(this).datepicker._selectDate(this, date);
        });

        $("input[data-type='date']").datepicker({
        showButtonPanel: true,
        closeText: 'OK',
        onSelect: function(date, inst) {
            changeMaterialHeader(headerHtml, moment(date, 'D-M-Y'));
        },
        });

        changeMaterialHeader(headerHtml, moment());
        $('input').datepicker('show');
      
});

function toggleProjType(e) {
    const tog = e.target;

    if($(tog).prop("checked")) {
        $(tog).attr('checked', true);
        $('#existing').css('display', 'none');
        $('#new').css('display', 'block');
    }else {
        $(tog).attr('checked', false);
        $('#existing').css('display', 'block');
        $('#new').css('display', 'none');
    }
}