<?php
date_default_timezone_set('America/Toronto');

//Section class_parent
	define("APP","app");

//Site path defined
	$sitePath = 'http://' . $_SERVER['HTTP_HOST'] . "/rams-project-automation";
	define("SITEPATH", $sitePath);

	if(isset($_REQUEST['uri']) && strpos($_REQUEST['uri'], 'export') === false){
		echo "<script>
		var SITEPATH = '".SITEPATH."';
		</script>";
	}

//Navigation Map
	$nav = array();
	$navSite = array();
	$navSite[] = array("name"=>"Upload","url"=>"upload","icon"=>"","type"=>"");
	$navSite[] = array("name"=>"Projections","url"=>"projections","icon"=>"","type"=>"");
	$navSite[] = array("name"=>"Logout","url"=>"logout","icon"=>"","type"=>"");

	//Final nav object
	$GLOBALS['nav'] = $navSite;

//Routing Init
	include("model/class.route.php");
	$route = new Route();
	$u = '';
	if(isset($_REQUEST['uri'])){
		$u = $_REQUEST['uri'];
	}

	//Final active page
	$GLOBALS['active'] = $u;

	//Route Map
	include("appRoute.php");
?>
