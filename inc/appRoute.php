<?php

/*PAGE VIEWS*/
/*Default*/
$route->add("", function() {
	$GLOBALS['active'] = "";
	$args='';

	getMetas($args);
	getClasses();
	getHead();

	include('view/landing-page.php');
	getFoot();
});
$route->add("/loggedin", function() {
	$GLOBALS['active'] = "";
	$args='';

	getMetas($args);
	getClasses();
	getHead();

	include('view/landing-page.php');
	getFoot();
});
$route->add("/upload", function() {
	$GLOBALS['active'] = "Upload";
	$args='';

	getMetas($args);
	getClasses();
	getHead();

	include('view/upload.php');
	getFoot();
});
$route->add("/export/.+", function($id) {
	getClasses();
	include('./controller/controller.export.php');
});
$route->add("/projections", function() {
	$GLOBALS['active'] = "Projections";
	$args='';

	getMetas($args);
	getClasses();
	getHead();

	include('view/projections.php');
	getFoot();
});
$route->add("/projection/.+/.+", function($id, $table) {
	$GLOBALS['active'] = "Projections";
	$args='';

	getMetas($args);
	getClasses();
	getHead();

	include('view/edit.php');
	getFoot();
});
$route->add("/submit", function() {
	$GLOBALS['active'] = "Submit";
	$args='';

	getMetas($args);
	getClasses();
	getHead();

	include('view/submit.php');
	getFoot();
});
$route->add("/logout", function() {
	session_start();
	if(session_destroy())
	{
		header("Location: http://".$_SERVER['HTTP_HOST']."/rams-project-automation");
	}
});
/*Default*/


/*Admin*/
	/*info*/
	$route->add("/admin/info", function() {
		$GLOBALS['active'] = "Admin Area";
		$args='';

		getMetas($args);
		getClasses();
		getHead();

		include('view/admin/info/info.php');
		getFoot();
	});
	$route->add("/admin/info/new", function() {
		$GLOBALS['active'] = "Admin Area";
		$args='';

		getMetas($args);
		getClasses();
		getHead();

		include('view/admin/info/new.php');
		getFoot();
	});
	$route->add("/admin/info/edit/.+/.+", function($infoType,$infoID) {
		$GLOBALS['active'] = "Admin Area";
		$args='';

		getMetas($args);
		getClasses();
		getHead();


		include('view/admin/info/edit.php');
		getFoot();
	});
	/*info*/
/*Admin*/


$route->submit();

function getMetas($args = null){

	if($args==null){
		$siteTitle = "Arup RAMs Project Automation";
		$siteImage = "/assets/img/logo.png";
		$siteDesc = "A tool to help project hourly projections.";

	}else{
		$siteTitle = $args['title'];
		$siteImage = $args['image'];
		$siteDesc = $args['description'];
	}

	define("siteTitle", $siteTitle);
	define("siteImage", $siteImage);
	define("siteDesc", $siteDesc);

	return;
}
function getClasses(){
	foreach (glob("model/*.php") as $filename){

		if(strpos($filename, 'route')!==false){
		}else{
			include $filename;
		}
	}
}
function getHead(){
	include("template/header.php");
	include('template/navigation.php');
	return;
}
function getFoot(){
	include("template/footer.php");
	return;
}

function makeSEOURL($toURL){
	//Lower case everything
	$toURL = strtolower($toURL);
	//Make alphanumeric (removes all other characters)
	$toURL = preg_replace("/[^a-z0-9_\s-]/", "", $toURL);
	//Clean up multiple dashes or whitespaces
	$toURL = preg_replace("/[\s-]+/", " ", $toURL);
	//Convert whitespaces and underscore to dash
	$toURL = preg_replace("/[\s_]/", "-", $toURL);
	return $toURL;
}

function undoSEOURL($toName){
	$toName = str_replace('-', ' ', $toName);
	$toName = preg_replace('/(?<!\s)-(?!\s)/', ' ', $toName);
	$toName = ucwords($toName);
	return $toName;
}

?>
