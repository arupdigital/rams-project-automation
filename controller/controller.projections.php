<?php
    // Get sessions
    $args = array(
        'args'=>array('session_id'=>''),
        'action'=>'select distinct',
        'target'=>'',
        'table'=>'taskrsrc',
        'file'=>''
    );

    $info = new Info;
    $sessions = $info->newSubmit($args);

    if(isset($_POST['session_id'])){
        
        $sessionId = $_POST['session_id'];
        $args = array(
            'args'=>array('session_id'=>$sessionId, '*'),
            'action'=>'select',
            'target'=>array('session_id'),
            'table'=>'task',
            'file'=>''
        );

        $info = new Info;
        $task = $info->newSubmit($args);

        $args = array(
            'args'=>array('session_id'=>$sessionId, '*'),
            'action'=>'select',
            'target'=>array('session_id'),
            'table'=>'taskrsrc',
            'file'=>''
        );

        $info = new Info;
        $taskrsrc = $info->newSubmit($args);

        $args = array(
            'args'=>array('session_id'=>$sessionId, '*'),
            'action'=>'select',
            'target'=>array('session_id'),
            'table'=>'rsrc',
            'file'=>''
        );

        $info = new Info;
        $rsrc = $info->newSubmit($args);

        $tables = [$task,$taskrsrc,$rsrc];
        $forDB = ['id','session_id','active','upload_date'];
    }
?>