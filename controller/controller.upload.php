<?php
use PhpOffice\PhpSpreadsheet\IOFactory;

$msg = '';
if(isset($_POST['action']) && isset($_FILES['file']) && $_FILES['file']['size']!=0){
      
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    $sessionID = generateRandomString();
    $date = new DateTime();
    $uploadDate = $date->format('Y-m-d H:i:sP');

    ini_set('auto_detect_line_endings', true);
    $tmpName = $_FILES['file']['tmp_name'];

    $inputFileName = $tmpName;
    $spreadsheet = IOFactory::load($inputFileName);
    $sheets = $spreadsheet->getSheetCount();

    $tables = ['task','taskrsrc','rsrc'];

    for($i=0; $i<(int)$sheets - 1; $i++){
        $sheetData = $spreadsheet->getSheet($i)->toArray(null, true, true, true);

        $headers = array();
        foreach($sheetData as $k=>$row){
            // set headers
            if($k==1){
                foreach($row as $v){
                    $headers[] = (string)$v;
                }
            }      

            // pump rows
            if($k>1){
                $obj = array();
                $obj['session_id'] = $sessionID;
                $key=0;
                foreach($row as $v){
                    $obj[''.$headers[$key].''] = (string)$v;
                    $key++;
                }
                $obj['active'] = 1;
                $obj['upload_date'] = $uploadDate;

                if(isset($obj)){

                    $args = array(
                        'args'=>$obj,
                        'action'=>'insert',
                        'target'=>'',
                        'table'=>$tables[$i],
                        'file'=>''
                    );

                    $info = new Info;
                    $res = $info->newSubmit($args);

                }
            }
        }
    }

    if($res==1){
        $msg = "File uploaded!";
        return $msg;
    }else{
        $msg = "No file uploaded...";
        return $msg;
    }
}

?>