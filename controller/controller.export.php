<?php

$tableNames = ['TASK','TASKRSRC','RSRC'];
$forDB = ['id','session_id','active','upload_date'];
$cellLetters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

    $sessionId = $id;
    $args = array(
        'args'=>array('session_id'=>$sessionId, '*'),
        'action'=>'select',
        'target'=>array('session_id'),
        'table'=>'task',
        'file'=>''
    );

    $info = new Info;
    $task = $info->newSubmit($args);

    $args = array(
        'args'=>array('session_id'=>$sessionId, '*'),
        'action'=>'select',
        'target'=>array('session_id'),
        'table'=>'taskrsrc',
        'file'=>''
    );

    $info = new Info;
    $taskrsrc = $info->newSubmit($args);

    $args = array(
        'args'=>array('session_id'=>$sessionId, '*'),
        'action'=>'select',
        'target'=>array('session_id'),
        'table'=>'rsrc',
        'file'=>''
    );

    $info = new Info;
    $rsrc = $info->newSubmit($args);

$tables = [$task,$taskrsrc,$rsrc];


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$spreadSheet = new Spreadsheet();

foreach($tableNames as $key=>$tableName){
    $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadSheet, $tableName);
    $spreadSheet->addSheet($myWorkSheet, $key);
    $sheet = $spreadSheet->getSheet($key);

    foreach($tables as $arrKey=>$d){
        if($arrKey==$key){
            $count=1;
            foreach($d as $i=>$row){
                $letter=0;
                foreach($row as $h=>$val){
                    if(!in_array($h, $forDB)){
                        if($i==0){
                            $sheet->setCellValue($cellLetters[$letter].$count, $h);
                            $sheet->setCellValue($cellLetters[$letter].($count+1), $val);
                        }else{
                            $sheet->setCellValue($cellLetters[$letter].($count+1), $val);
                        }
                        $letter++;
                    }
                }
                $count++;
            }
        }
    }
}

$spreadSheet->removeSheetByIndex(3);
$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadSheet, 'USERDATA');
$spreadSheet->addSheet($myWorkSheet, 3);
$sheet = $spreadSheet->getSheet(3);
$sheet->setCellValue('A1', "'user_data");
$sheet->setCellValue('A2', "'UserSettings Do Not Edit");
$sheet->setCellValue('A3', "'DurationQtyType=QT_Day
ShowAsPercentage=0
SmallScaleQtyType=QT_Hour
DateFormat=dd'-'mmm'-'yy
CurrencyFormat=Dollar
");

$spreadSheet->setActiveSheetIndex(0);

$filename = 'sample-'.time().'.xlsx';

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

$writer = IOFactory::createWriter($spreadSheet, 'Xlsx');
$writer->save('php://output');

?>